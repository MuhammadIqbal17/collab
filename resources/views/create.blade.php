@extends('master')

@section('content')
<div class="m-3">
  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title">Buat Postingan</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/postingan" method="POST">
    @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Isi</label>
          <input type="text" class="form-control" id="isi" name="isi" placeholder="Apa yang anda pikirkan?">
          @error('judul')
            <div class="alert alert-danger">Form Isi harus diisi</div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-info">Buat</button>
      </div>
    </form>
  </div>
</div>

@endsection
