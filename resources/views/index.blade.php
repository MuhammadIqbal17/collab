@extends ('master')

@section('content')
<div class="m-3">
  <a class="btn btn-info mb-1" href="/postingan/create">Buat Postingan</a>
</div>

@forelse($postingan as $key => $postingan)
<div class="card card-widget">
  <div class="card-header">
    <div class="user-block">
      <img class="img-circle" src="{{asset('adminlte/dist/img/user1-128x128.jpg')}}" alt="User Image">
      <span class="username mt-2"><a href="#">Jonathan Burke Jr.</a></span>
    </div>
    <!-- /.user-block -->
    <div class="card-tools">
      <form action="/postingan" method="post">
        @csrf
        @method('DELETE')
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
        </button>
      </form>
    </div>
    <!-- /.card-tools -->
  </div>
  <!-- /.card-header -->
  <div class="card-body">
  <!-- post text -->
    <p>{{ $postingan -> isi }}</p>
    <!-- Social sharing buttons -->
    <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
    <span class="float-right text-muted">45 likes - 2 comments</span>
  </div>
  <!-- /.card-footer -->
  <div class="card-footer">
    <form action="#" method="post">
      <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/dist/img/user4-128x128.jpg')}}" alt="Alt Text">
      <!-- .img-push is used to add margin to elements next to floating images -->
      <div class="img-push">
        <input type="text" class="form-control form-control-sm" placeholder="Press enter to post comment">
      </div>
    </form>
  </div>
  <!-- /.card-footer -->
</div>
@empty
  <tr>
    <td colspan="4" align="center">Tidak ada pertanyaan</td>
  </tr>
@endforelse

@endsection
