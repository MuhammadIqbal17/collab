<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeKomentarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_komentar', function (Blueprint $table) {
          $table->bigIncrements('id');
           $table->unsignedBigInteger('profile_id');
          $table->unsignedBigInteger('komentar_id');
          $table->unsignedBigInteger('jumlah_like');
          $table->foreign('profile_id')->references('id')->on('profile');
          $table->foreign('komentar_id')->references('id')->on('komentar');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_komentar');
        $table->dropForeign('komentar_id');
        $table->dropForeign('profile_id');
        $table->dropColumn('jumlah_like');
        $table->dropColumn('komentar_id');
        $table->dropColumn('profile_id');
    }
}
