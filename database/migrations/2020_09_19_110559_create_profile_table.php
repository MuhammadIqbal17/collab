<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('Nama');
             $table->string('Desc')->nullable();
             $table->date('Birthday')->nullable();
             $table->binary('Picture')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
        $table->dropColumn('Picture');
         $table->dropColumn('Birthday');
         $table->dropColumn('Desc');
         $table->dropColumn('Nama');
         $table->dropColumn('id');
    }
}
