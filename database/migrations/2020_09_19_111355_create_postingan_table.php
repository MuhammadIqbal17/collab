<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostinganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postingan', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('Isi');
          $table->string('Photo')->nullable();
          $table->unsignedBigInteger('profile_id');
          $table->foreign('profile_id')->references('id')->on('profile');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postingan');
        $table->dropForeign('profile_id');
        $table->dropColumn('profile_id');
        $table->dropColumn('Photo');
        $table->dropColumn('isi');
        $table->dropColumn('id');
    }
}
