<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKomentarIdToPostingan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('postingan', function (Blueprint $table) {
          $table->unsignedBigInteger('komentar_id');
          $table->foreign('komentar_id')->references('id')->on('komentar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('postingan', function (Blueprint $table) {
          $table->dropForeign('komentar_id');
            $table->dropColumn('komentar_id');
            //
        });
    }
}
