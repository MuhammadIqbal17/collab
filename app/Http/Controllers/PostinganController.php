<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class PostinganController extends Controller
{
  public function __construct(){
      $this->middleware('auth');
  }

  public function index(){
      $postingan = DB::table('postingan')->get();
      return view('index', compact('postingan'));
  }

  public function create(){
      return view('create');
  }

  public function store(Request $request){
      $request->validate([
          'isi' => 'required'
      ]);

      $query = DB::table('postingan')->insert([
           "isi" => $request["isi"],
      ]);

      return redirect('/postingan')->with('success', 'Berhasil!');
  }

  public function destroy($postingan_id){
      $query = DB::table('postingan')->where('id', $postingan_id)->delete();

      return redirect('/pertanyaan');
  }
}
